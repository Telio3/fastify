import path from 'path';

export default {
    cert: path.join(__dirname, '../../ssl/server.cert'),
    key: path.join(__dirname, '../../ssl/server.key'),
};
