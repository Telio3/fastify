export default {
    cert: '',
    key: '',
    portHttp: 80,
    portHttps: 443,
};
