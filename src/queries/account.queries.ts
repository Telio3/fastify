import { QueryResult } from 'pg';
import { pool } from '../database/index.js';
import { Account, IAccount } from '../database/models/account.model.js';

export const getAccountById = (accountId: number): Promise<QueryResult<IAccount>> => {
    const query = {
        text: 'SELECT * FROM account WHERE id = $1',
        values: [accountId],
    };

    return pool.query(query);
};

export const getAccountByEmail = (email: string): Promise<QueryResult<IAccount>> => {
    const query = {
        text: 'SELECT * FROM account WHERE email = $1',
        values: [email],
    };

    return pool.query(query);
}

export const insertAccount = (body: IAccount): Promise<QueryResult<IAccount>> => {
    const account = Account.from(body);

    const query = {
        text: 'INSERT INTO account (username, email, password) VALUES ($1, $2, $3) RETURNING *',
        values: [account.username, account.email, Account.hashPassword(account.password)],
    };

    return pool.query(query);
}

export const updateUsernameAccountById = (accountId: number, username: string): Promise<QueryResult<IAccount>> => {
    const query = {
        text: 'UPDATE account SET username = $1 WHERE id = $2 RETURNING *',
        values: [username, accountId],
    };

    return pool.query(query);
}

export const updateDescAccountById = (accountId: number, desc: string): Promise<QueryResult<IAccount>> => {
    const query = {
        text: 'UPDATE account SET description = $1 WHERE id = $2 RETURNING *',
        values: [desc, accountId],
    };

    return pool.query(query);
}

export const deleteAccountById = (accountId: number): Promise<QueryResult<IAccount>> => {
    const query = {
        text: 'DELETE FROM account WHERE id = $1 RETURNING *',
        values: [accountId],
    };

    return pool.query(query);
}
