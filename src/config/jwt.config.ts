import jwt from 'jsonwebtoken';
import { DoneFuncWithErrOrRes, FastifyReply, FastifyRequest } from 'fastify';

export const createJwtToken = (id: number): string => {
    const jwtToken = jwt.sign(
        {
            sub: id.toString(),
            exp: Math.floor(Date.now() / 1000) + 5
        },
        process.env.JWT_SECRET!
    );

    return jwtToken;
}

export const extractToken = async (request: FastifyRequest, reply: FastifyReply, done: DoneFuncWithErrOrRes): Promise<void> => {
    try {
        const token = request.headers.authorization?.split(' ')[1];

        if (!token) {
            reply.status(401).send('Invalid token');
            return;
        }

        jwt.verify(token, process.env.JWT_SECRET!, { ignoreExpiration: true });

        done();
    } catch (e) {
        reply.status(401).send('Invalid token');
    }
}
