import { FastifyInstance } from 'fastify';
import { extractToken } from '../config/jwt.config';

import account from './account.routes';
import authRoutes from './auth.routes';

export default async function routes(app: FastifyInstance): Promise<void> {
    app.register(authRoutes, {
        prefix: '/auth'
    });

    app.register(account, {
        prefix: '/account'
    });
}
