import { FastifyInstance } from 'fastify';
import { extractToken } from '../config/jwt.config';
import { getAccount, postAccount, putUsernameAccount, putDescAccount, deleteAccount } from '../controllers/account.controller';

export default async function accountRoutes(app: FastifyInstance): Promise<void> {
    app.get('/:id', { preHandler: extractToken }, getAccount);
    app.post('/', postAccount);
    app.put('/:id/username', putUsernameAccount);
    app.put('/:id/desc', putDescAccount);
    app.delete('/:id', deleteAccount);
    // app.post('/avatar', postAccountPicture);
}
