import { insertAccount, getAccountById, updateUsernameAccountById, updateDescAccountById, deleteAccountById } from '../queries/account.queries';
import path from 'path';
import multer from 'multer';
import { FastifyReply, FastifyRequest } from 'fastify';
import { IAccount } from '../database/models/account.model';
type File = Express.Multer.File;

export const getAccount = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const { id } = request.params as { id: number };
        const response = await getAccountById(id);

        if (response.rowCount === 0) {
            reply.status(404).send('Account not found');
        } else {
            reply.status(200).send(response.rows[0]);
        }
    } catch (e) {
        reply.status(500).send(e);
    }
};

export const postAccount = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const body = request.body as IAccount;
        const newAccount = await insertAccount(body);
        reply.status(200).send(newAccount.rows[0]);
    } catch (e) {
        reply.status(500).send(e);
    }
};

export const putUsernameAccount = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const { id } = request.params as { id: number };
        const { username } = request.body as { username: string };
        const response = await updateUsernameAccountById(id, username);
        const account = response.rows[0];
        reply.status(200).send(account);
    } catch (e) {
        reply.status(500).send(e);
    }
}

export const putDescAccount = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const { id } = request.params as { id: number };
        const { description } = request.body as { description: string };
        const response = await updateDescAccountById(id, description);
        const account = response.rows[0];
        reply.status(200).send(account);
    } catch (e) {
        reply.status(500).send(e);
    }
}

export const deleteAccount = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const { id } = request.params as { id: number };
        const response = await deleteAccountById(id);
        reply.send(response);
    } catch (e) {
        reply.status(500).send(e);
    }
}

// const upload = multer({
//     storage: multer.diskStorage({
//         destination: (_, __: File, cb: Function) => {
//             cb(null, path.join(__dirname, '../../public/images/avatars'));
//         },
//         filename: (_, file: File, cb: Function) => {
//             cb(null, `${Date.now()}-${file.originalname}`);
//         },
//     }),
// });

// export const postAccountPicture = [
//     upload.single('avatar'),
//     async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
//         try {
//             reply.status(200).send(`https://127.0.0.1:443/images/avatars/${request.file?.filename}`);
//         } catch (e) {
//             reply.status(500).send(e);
//         }
//     },
// ];
