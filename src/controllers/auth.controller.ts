import { getAccountByEmail } from "../queries/account.queries";
import { Account } from "../database/models/account.model";
import { FastifyReply, FastifyRequest } from "fastify";
import { createJwtToken } from "../config/jwt.config";

export const login = async (request: FastifyRequest, reply: FastifyReply): Promise<void> => {
    try {
        const { email, password } = request.body as { email: string, password: string };

        const accountObject = await getAccountByEmail(email);

        if (accountObject.rowCount === 1) {
            const account = Account.from(accountObject.rows[0]);

            const match = await account.comparePassword(password);

            if (match) {
                const token = createJwtToken(account.id);

                reply.setCookie('jwt', token, {
                    httpOnly: true,
                    secure: true,
                    sameSite: 'strict',
                    maxAge: 60 * 60 * 24 * 7
                });

                reply.status(200).send({
                    token,
                    account
                });
            } else {
                reply.status(401).send('Invalid password');
            }
        } else {
            reply.status(401).send('Invalid email');
        }
    } catch (e) {
        reply.status(500).send(e);
    }
}
