import fastify from "fastify";
import fs from 'fs'
import cors from '@fastify/cors';
import cookie from '@fastify/cookie'
import dotenv from 'dotenv';
import path from 'path';
import './database';
import routes from './routes';
import conf from './environment';

const env = conf[process.env.NODE_ENV as 'development' | 'production'];

dotenv.config();

export const app = fastify({
    http2: true,
    https: {
        allowHTTP1: true,
        key: fs.readFileSync(env.key),
        cert: fs.readFileSync(env.cert)
    }
});

app.register(cors);
app.register(cookie);

app.register(require('@fastify/static'), {
    root: path.join(__dirname, '../public')
});

app.register(routes, {
    prefix: '/api',
});

app.listen({ port: 443 })
