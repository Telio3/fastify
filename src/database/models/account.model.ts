import bcrypt from 'bcrypt';

export interface IAccount {
    id: number;
    email: string;
    password: string;
    username: string;
    description: string;
    created_at: Date;
}

export class Account implements IAccount {
    public id: number;
    public email: string;
    public password: string;
    public username: string;
    public description: string;
    public created_at: Date;

    constructor(id: number, email: string, password: string, username: string, description: string, created_at: Date) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.username = username;
        this.description = description;
        this.created_at = created_at;
    }

    public static from(obj: IAccount): Account {
        return new Account(obj.id, obj.email, obj.password, obj.username, obj.description, obj.created_at);
    }

    public static hashPassword(password: string): string {
        return bcrypt.hashSync(password, 10);
    }

    public comparePassword(password: string): Promise<boolean> {
        return bcrypt.compare(password, this.password);
    }
}